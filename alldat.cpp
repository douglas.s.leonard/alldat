//alldat Douglas Leonard, 2021, CUP IBS, all rights reserved

//Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed
//on the licenses of the linked code or binaries  (such as reverse engineering restrictions),
//and that this same exception applies to all modified versions of this work.


#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <unistd.h>
#include <getopt.h>

#include "alldat.h"

using namespace std;

int main(int argc, char *argv[])
        {

    string in_histname; //histogram for input root files.
    /*
     * Option Parsing
     */
    int c;
    double realtime_arg { };
    double livetime_arg { };
    double gen_event_arg { };
    bool synerr = false;
    vector<string> sumfiles;
    vector<int> sumsigns;

    while (1) {
        static struct option long_options[] = {
                { "histname", required_argument, 0, 'h' },
                { "realtime", required_argument, 0, 'r' },
                { "livetime", required_argument, 0, 'l' },
                { "genevents", required_argument, 0, 'g' },
                { "add", required_argument, 0, 'a' },
                { "subtract", required_argument, 0, 's' },
                { "help", no_argument, 0, 'x' },
        };

        c = getopt_long(argc, argv, "h:r:l:g:a:s:",
                long_options, 0);
        if (c == -1) { // no more options
            break;
        }
        cout << endl;
        switch (c) {
            case 'h':
                in_histname = optarg;
                break;
            case 'a':   // files to add
                sumfiles.push_back(optarg);
                sumsigns.push_back(1);
                break;
            case 's':   //  files to subtract
                sumfiles.push_back(optarg);
                sumsigns.push_back(-1);
                break;
            case 'l':   // can't apply these to data, until just before writing.
                livetime_arg = stod(optarg);
                break;
            case 'r':
                realtime_arg = stod(optarg);
                break;
            case 'g':
                gen_event_arg = stod(optarg);
                break;
            case '?':
                synerr = true;
                break;
            case 'x':
                synerr = true;
                break;
            default:
                cout << "   Oops, option parsing has a bug.  Getopt returned " << c << endl;
                return (1);
        }
    }
    if (argc - optind < 2) { // not enough files specified
        synerr = true;
    }
    if (synerr) {
        cout << endl;
        cout << "Usage:alldat [OPTIONS]  <infile1>.<ext1> [-a <infile1>.<ext2>] ... [-s <infileN>.<extN>] <outfile>.<ext>" << endl;
        cout << endl;
        cout << "    .ext's can designate any supported input or output type, presently: .chn, .spe, .spc (input only) or .root" << endl;
        cout << endl;
        cout << "    Options: " << endl;
        cout << "    -a or -s before optional filenames indicates addition or subtraction of spectrum, times and gen_events" << endl;
        cout << "         Any number of files can be added or subtracted " << endl;
        cout << "    -h,-histname=<name> , Required for infile.root to specify the histogram name to read" << endl;
        cout << "          all TH1-derived root histogram types are supported automatically." << endl;
        cout << "    -l,--livetime=<seconds> manually set livetime of output" << endl;
        cout << "    -r,--realtime=<seconds> manually set realtime of output" << endl;
        cout << "    -g,--genevents=<seconds> manually set gen_events of output" << endl;
        cout << endl;
        cout << "    Conversion between same format may be useful to unify histogram names," << endl;
        cout << "        to generate the metadata structure, or to manually edit metadata." << endl;
        cout << "    Conversion from root preserves only one requested histogram (and metadata if found)" << endl;
        cout << "    The output root histogram name will always be \"spec\"" << endl;
        cout << endl;
        return (1);
    }
    /*
     * End of option parsing
     */

    std::string ext;

    alldat data, newdata;
    data.infilename = argv[optind];
    cout << "Reading file " << data.infilename << endl;
    data.histname = in_histname;

    ext = data.infilename.substr(data.infilename.find_last_of(".") + 1);
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower); // convert to lower case
    if (ext == "root" && data.histname == "") {
        cout << "a root input file was specified without a histogram name" << endl;
        cout << "Please provide one with the -h or--histname=name " << endl;
        return 1;
    }
    cout << "\n";
    if (!data.flexread()) {
        exit(EXIT_FAILURE);
    }

    cout << endl;
    cout << "Input file summary: " << endl;
    data.print_summary();

    for (size_t idx = 0; idx < sumfiles.size(); idx++) {
        newdata.infilename = sumfiles[idx];
        newdata.histname = in_histname;

        ext = newdata.infilename.substr(data.infilename.find_last_of(".") + 1);
        std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower); // convert to lower case
        if (ext == "root" && newdata.histname == "") {
            cout << "a root input file was specified without a histogram name" << endl;
            cout << "Please provide one with the -h or--histname=name " << endl;
            return 1;
        }

        if (!newdata.flexread()) {
            exit(EXIT_FAILURE);
        }


// no consistency checks between data sizes here.
// At your own risk! // only+= and -= are defined, not general math.
        if (sumsigns[idx] == 1) {
            cout << "Adding file " << newdata.infilename << endl;
            newdata.print_summary();
            data += newdata;
        } else if (sumsigns[idx] == -1) {
            cout << "Subtracting file " << newdata.infilename << endl;
            newdata.print_summary();
            data -= newdata;
        } else {
            cout << "This is a bug in all2all.cpp" << endl;
            return (1);
        }
    }

    data.outfilename = argv[argc - 1];
// apply user metadata overrides
    if (realtime_arg) {
        data.spc_realtime = realtime_arg;
    }
    if (livetime_arg) {
        data.spc_livetime = livetime_arg;
    }
    if (gen_event_arg) {
        data.gen_event = gen_event_arg;
    }
    //convert to to other formats
    data.spc2chn();
    data.spc2UA();  //not presently needed, just in case

// the rest is obvious
    data.flexwrite();
    cout << endl;
    cout << "Output file summary: " << endl;
    data.print_summary();
    cout << endl;
    return (0);
}
