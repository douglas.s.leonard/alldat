//histstats Douglas Leonard, 2020, CUP IBS, all rights reserved

// Reads file of any recognized file extension and outputs summary information.

//Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed
//on the licenses of the linked code or binaries  (such as reverse engineering restrictions),
//and that this same exception applies to all modified versions of this work.


#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "alldat.h"


using namespace std;

int main(int argc, char* argv[])
{
    int c;
    bool synerr = false;
    vector<string> sumfiles;
    vector<int> sumsigns;
    string in_histname; //histogram for input root files.

/*
 * Option Parsing
 */
    while (1) {
        static struct option long_options[] = {
                { "histname", required_argument, 0, 'h' },
                { "help", no_argument, 0, 'x' },
        };

        c = getopt_long(argc, argv, "h:",
                long_options, 0);
        if (c == -1) { // no more options
            break;
        }
        switch (c) {
            case 'h':
                in_histname = optarg;
                break;
            case '?':
                synerr = true;
                break;
            case 'x':
                synerr = true;
                break;
            default:
                cout << "   Oops, option parsing has a bug.  Getopt returned " << c << endl;
                return (1);
        }
    }
    if (argc - optind != 1) { // not enough files specified
        synerr = true;
    }
    if (synerr) {
        cout << endl;
        cout << "Usage:histstats [OPTIONS] input1.ext" << endl;
        cout << endl;
        cout << "    Options: -h,-histname=<name> , Required for infile.root to specify histogram name to read" << endl;
        cout << "    Filenames can be any type with input  support : .chn, .spe , .spc .root, maybe more" << endl;
        cout << "    Conversion fromm .root to .root may be useful to unify histogram names, to the default" << endl;
        cout << "       ... or to generate the metadata structure." << endl;
        cout << endl;
        return (1);
    }
/*
 * End of option parsing
 */


    alldat data ;
    data.infilename=argv[optind];
    data.histname=in_histname;
    if (!data.flexread()) {
        exit(EXIT_FAILURE);
    }
    data.print_summary();
}
