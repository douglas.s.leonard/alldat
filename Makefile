#Mostly all purpose Makefile, D. Leonard 2017
#Includes header depends, library install, and directory dependencies

local_bin ?= ./bin
local_lib ?= ./lib

INSTALL_PATH   = $(local_bin)/
LIB_INSTALL_PATH = $(local_lib)
SHELL = /bin/bash


ifndef ROOT_PATH
ifndef ROOTSYS
$(error ROOTSYS is not set)
endif
ROOT_PATH ?= $(ROOTSYS)
endif

# library archiving command
AR             = ar
#library archiving flags
ARFLAGS        = rv
#General compile flags... optimizations etc.
OPT            = -O2
#Fortran flags
FFLAGS         = $(MYFFLAGS) -fbackslash -g
#C Flags
CFLAGS         = $(MYCPPFLAGS) -mfpmath=sse -g
#-fdebug-prefix-map=..=$(readlink -f ..)
#C++Flags
CXXFLAGS        =  $(MYCPPFLAGS)  -g -std=c++11
#Linker search path
LD_PATH        = -L$(local_lib) $(shell $(ROOT_PATH)/bin/root-config --libs --glibs --cflags --ldflags --auxlibs --auxcflags) -lstdc++
#Header search path:
INCLUDES       = -I$(ROOT_PATH)/include/
#Linker executeable
FC             :=  $(LD_PATH) $(INCLUDES)
#C++ compiler, same as CC
CXX = $(CC) $(LD_PATH) $(OPT) $(INCLUDES)
#C compiler
CC              := $(CC) $(LD_PATH) $(OPT) $(INCLUDES)
#Linker flags
LDFLAGS        = -g
LD             := $(CXX) $(LD_PATH)
#Complete linker command
LINK           = $(LD) $(LDFLAGS)



#-----------------------------------------------------------

#Program specific stuff, edit here:

#Objects needed for executables
OBJS  =  liballdat.o fileio.o

#Objects needed for libraries
LIB_alldat_OBJ = liballdat.o fileio.o

#Libararies needed for executables
INC_LIBS  =

#Executeables to build
EXE     = alldat histstats roisum  

#Libraries to build
LIBS	= liballdat.a

#Directories where other codes should be updated
# like to update library compilation
# directories are relative to $(src_path) and their names
SOURCE_DIRS =

install: all
	mv $(EXE) $(INSTALL_PATH)
	mv $(LIBS)  $(LIB_INSTALL_PATH)


all: $(LIBS) $(EXE)
	@echo "Use 'make install' to install libraries."

$(EXE): %: %.o $(OBJS) $(SOUCE_DIRS)
	$(LD) $(LDFLAGS) $(OBJS) $< $(INC_LIBS) $(LD_PATH) -o $@


liballdat.a: $(LIB_alldat_OBJ)
	   $(AR) $(ARFLAGS) $@ $^


# Don't edit below here.... unless you want to.

$(SOURCE_DIRS):
	$SHELL
	@$(SHELL) make all install -C $(code_home)/$* $(MFLAGS)


# make automatic dependencies (header or included file dependancies)
-include $(OBJS:.o=.d)



# generic object rules

%.o : %.for Makefile
	$(FC) $(FFLAGS) -c $*.for
	@echo "done: $@"

%.o : %.f Makefile
	$(FC) $(FFLAGS) -c $*.f
	@echo "done: $@"

%.o: %.c Makefile
	$(CC) $(CFLAGS) -c $*.c -o $*.o
	$(CC) $(CFLAGS) -MM $*.c > $*.d

%.o: %.cpp Makefile
	$(CXX) $(CXXFLAGS) -c $*.cpp -o $*.o
	$(CXX) $(CXXFLAGS) -MM $*.cpp > $*.d
	@echo "done: $@"

%.o: %.cc Makefile
	$(CXX) $(CXXFLAGS) -c $*.cc -o $*.o
	$(CXX) $(CXXFLAGS) -MM $*.cc > $*.d
	@echo "done: $@"


.PHONY: all install backup clean

backup:
	mkdir backup
	cp *.for *.c *.cc *.h *.cpp *.f backup

clean:
	rm -f $(wildcard *.o *.d *.a core) $(EXE)

$(shell mkdir -p $(local_lib) $(local_bin))
