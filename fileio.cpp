//fileio.cc dleonard 2015,

// Copyright (C) 2015 Doug Leonard
//
//Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed
//on the licenses of the linked code or binaries  (such as reverse engineering restrictions),
//and that this same exception applies to all modified versions of this work.


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <memory>
using namespace std;

/*
   Purpose: Read entire contents of file into memory
   Arguments:  infilename: name of file to be read
   Returns a unique_ptr that will be autodeleted when out of scope.
*/
unique_ptr<char []> readbinfile(std::string infilename)
{
    unique_ptr<char []> memblock;
    streampos size;
    ifstream file {infilename.c_str(), ios::in|ios::binary|ios::ate};
    if (file.is_open()) {
        size = file.tellg();
//    memblock = malloc(size+1); // alternative
        memblock = unique_ptr <char []> {new char[(size_t)size+(size_t)1]};
        file.seekg (0, ios::beg);
        file.read (memblock.get(), size);
        file.close();
        cout << "\nFile " << infilename << " read to memory.\n";
        // This is not an object member function, so need to "move" memory ownership to caller.
        return move(memblock);
    } else {
        cout << "\n Unable to open file" << infilename << '\n';
        return std::unique_ptr<char []>(nullptr);
    }
}
