// alldat.h Douglas Leonard 2015, CUP IBS, all rights reserved.
// Copyright (C) 2015 Doug Leonard
//
//
//Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed
//on the licenses of the linked code or binaries  (such as reverse engineering restrictions),
//and that this same exception applies to all modified versions of this work.

#ifndef alldat_h
#define alldat_h
#include <string>
#include <iostream>
#include <array>
#include <memory>
#include <map>
#include <TH1.h>
#define DESCLEN 63
#define TH1F_TYPE 1
#define TH1I_TYPE 2

using namespace std;
//class alldat& operator+=(class alldat& lhs, const class alldat& rhs);

class alldat
{
public:
// Memory ownership is managed by unique_ptrs which are automatically deleted when the object scope dies.
// Do not delete the raw pointers from calling code.
//

    short ft1,ft2;  // file type identifiers 2 bytes.
//  short int chnum_ptr=(unique_ptr<short>)new short int;
    short chnum;
    short startchan;  // 2 bytes
    short mca, seg; // mca dev type and segment number
// spc holds real and live time in two places, with differing precision:
//    unique_ptr<float> spc_real_livetime_ptr{new float}; //sec, from the primary spc header 4 bytes
    float spc_livetime; //sec, from the primary spc header 4 bytes
    float spc_realtime; //sec, from the primary spc header 4 bytes

    int realtime_20ms{0},livetime_20ms{0}; // in chn format, 20 ms units

// next two not fully implemented (never recalculated) and not seemingly useful:
    char   spc_acq_char_livetime; // from acq record in spc
    char   spc_acq_char_realtime; // from acq record in spc

//one will get set; the other copied/converted so both types always exist.
// memory is allocated in fblock and iblock as needed.  These are just pointers:
    int *datahist=nullptr;      // 4 byte integer hist data
    float *fdatahist=nullptr;   // 4 byte real histogram data for float spc file
    TH1 *roothist=nullptr; // TH1F or TH1I only available after explicit method calls.

    float ecal0, ecal1, ecal2; // energy cal polynomial parameters from metadata
    float x_offset {0};   //  x_offset of 0 bin. particularly for root, used by gdfit.
    float root_cal0{0}, root_cal1 {1}; // linear energy cal derived from root histogram itself.
    float sigcal0, sigcal1, sigcal2; // energy cal polynomial parameters

// See time format in comments below:
    char spc_cstartdate[11] {0}; // asci start date in spc format  DD-MMM-YY1
    char spc_cstarttime[9] {0}; // asci start time in spc format   HH:MM:SS
    char defspec[10] {0};  // maybe unused ?

    char detdesc[DESCLEN] {0};   // det description string
    char sampdesc[DESCLEN] {0};   // sample description string

//length of description strings seems to need unsigned to include 128
// but not well documented
    char detdesc_len{0}, sampdesc_len{0};

//***** UA time structure, separated array of values *****
// 0:year 1:month 2:day 3:hour 4:min 5:sec 6:unused
// should probably just be replaced with c tm structure, although that's weird too.
    std::array <Int_t,7> istarttime{{0}};

// Events generated in MC data:
    Int_t gen_event {0};

    std::string infilename{},outfilename{};
    std::string histname=""; // for root files (when implemented);
    
    int readspc();   // reads chn file
    int readchn();   // reads spc file
    int readroot();   // reads root file
    int readspe();
    void writechn();  // etc...
    void writeroot();
    void writespe();  // write spe asci file format.
    int  flexread();  // reads file based on file extension
    void  flexwrite();  // writes file based on file extension

    // print a summary of data:
    void print_summary();

// convert spc time structures to others
    void spc2chn();
    void chn2spc();
    void spc2UA();
// copy to/from fdatahist <--> datahist
    void float2int();
    void int2float();
// make a TH1F or TH1I, either will be pointed to with roothist  (TH1*)
    void make_TH1F();
    void make_TH1I();
    
    // addition and subtraction operators:
    // note, can only add to existing object, not create a new one ( no a = b + c )
    alldat& operator+=(class alldat& );  // keeps start time of left hand side
    alldat& operator-=(class alldat& );  // keeps start time of left hand side

    // no copy or assignment construction allowed:
    alldat& operator =( const alldat & ) = delete;
    alldat ( const alldat & ) = delete;
    // default default constructor:
    alldat();
    ~alldat() {}; //  using managed memory, nothing to delete!! C++11 is great.


//SUMMARY OF TIME FORMATS
    //0123456789
    //DD-MMM-YY1  spc_cstartdate  // Month as letters ex: Jan
    //DDMMMYY1    chn_cstartdate // Month as letters ex: Jan
    //DD/MM/YYYY    spe_cstartdate  // Month as numbers like inst istarttime
    //
    //HH:MM:SS    spc_cstarttime
    //HHMM        chn_cstarttime
    //SS          chn_cstartsec
    //int istarttime[7] ={YEAR, MONTH, DAY, HOUR, MIN, SEC, 100TH'S??}  //"UA" format


private:
// memmory to hold file  (spc or otherwise)
    char *spcmemblock {};

    void get_root_hist(TH1 *h1);
    char root_type{};

//**** chn specific data structures *********
// almost same as spc, just for output use
// Not gauraunteed available.

//date strings for chn format
// chars 0:1 hour, 2:3 min

    char chn_cstarttime[5] {0};
    char chn_cstartsec[4] {0};
    // 0,1: DD, 2--4: MMM(letters), 5--6: YY, 7: 1 for after 2000 else 0
    char chn_cstartdate[9]= {'0','1','J','a','n','0','0','1','\0'};
    char spe_cstartdate[9] {0}; // asci start date in spe format  DD/MM/YY
    map<const string, int> months; // names of months.

    // Decalare all memory with object ownership, but it won't all be allocated.
    //  Some items are only allocated if not already containted in the file memory.
    unique_ptr <char []> file_mem;  // declare pointer to receive file contents.
    unique_ptr<int []> iblock;  // declare memory for integer data copy if needed (for float file), allocate later.
    unique_ptr<float []> fblock; // declare memory for float data copy if needed (for int file), allocate later.
    unique_ptr<TH1> roothist_ptr; // declare memory for root histogram, allocate later.

// declare memory to hold floating acquisition times (Spc format) and spc date formats in cases where it's not
// included in the file memory (non spc file).  Allocated as needed.
//    unique_ptr<float> spc_real_realtime_mem;
//    unique_ptr<float> spc_real_livetime_mem;
//    char spc_cstartdate_mem[11] {0}; // declare memory spc date string if needed.
//    char spc_cstarttime_mem[9] {0}; // declare memory spc time string if needed.

// list of root branch definitions
    vector<tuple<string, void*, string>> brvec;

// changes word count to byte offset:
    auto w2b(int rec) -> int; // function declaration
// generic function to make any TH1 derived histogram
// The specialized public version just set the constructor pointer and call it
    void make_TH1();
};

//For pointer to TH1 derived class constructor:
typedef void (*TH1X_type)(const char *, const char *, Int_t , const Double_t *);

#endif

