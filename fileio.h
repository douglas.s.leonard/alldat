#ifndef fileio_h
#define fileio_h
#include <memory>
using namespace std;
unique_ptr<char []> readbinfile(std::string filename);
#endif
