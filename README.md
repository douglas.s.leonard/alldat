alldat data class utilities for reading, writing, and manipulating ORTEC data files.  
-------------
Copyright 2015 Doug Leonard, IBS all rights reserved.   
Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed on the licenses of the linked code or binaries (such as reverse engineering restrictions), and that this same exception applies to all modified versions of this work. 

The point is I place no restrictions on how you link to it (and I'm skeptical of the legal argument that I should be able to). I don't care about tivioization, and while I'm not an expert on the legal details, it seems patently absurd to provide open-ended license that allows some future, yet-unamed, foundation committee members to change the license of my code at will, hence no future version clause.  lgpl 2.1 should be sufficient to allow linking to gpl v3 and later code, but otherwise not inclusion within it.  

-------------
### **See [the wiki](https://gitlab.com/douglas.s.leonard/alldat/-/wikis/home) for the latest information and instructions.  This file may be out of date.**    

alldat.h Defines a data class for holding ORTEC or similar spectral data, especially from HPGE detectors
liballdat.cpp Defines file readers writers and type conversions.  
     
In particular the alldat::flexread() function can read any supported file extension.  

fileio.cpp used for reading file into a memory block.  

Supporeted files types for reading:  
  .chn .Spc .root .spe  
  (alldat::histname must be specified for reading root files)  

Supported file types for writing:  
  .chn .root .spe  

Recents updates:  
Now reads chn files, and reads and write root and spe files. and flexread() and flexwrite() auto detects file extension.  

Operator += and -+ now defined for adding and subtracting two files.  

Now a single executable performs most conversions and manipulations.  
Type the name with no arguments for usage:  

alldat   reads any file or files, sums and/or subtracts them and outputs to any supported type.  
         Can be used to convert from root to root to unify histogram names to the default (only specified histogram is kept).  

```
Usage:alldat [OPTIONS]  <infile1>.<ext1> [-a <infile1>.<ext2>] ... [-s <infileN>.<extN>] <outfile>.<ext>  

    .ext's can designate any supported input or output type, presently: .chn, .spe, .spc (input only) or .root

    Options: 
    -a or -s before optional filenames indicates addition or subtraction of spectrum, times and gen_events
         Any number of files can be added or subtracted 
    -h,-histname=<name> , Required for infile.root to specify the histogram name to read
          all TH1-0derived root histogram types are supported automatically.
    -l,--livetime=<seconds> manually set livetime of output
    -r,--realtime=<seconds> manually set realtime of output
    -g,--genevents=<seconds> manually set gen_events of output

    Conversion between same format may be useful to unify histogram names,
        to generate the metadata structure, or to manually edit metadata.  
    Conversion from root preserves only one requested histogram (and metadata if found)
    The output root histogram name will always be "spec"
```

roisum   sums spectrum in input file using ROI from sumbin.txt if it exists.  

histstats:  just print information about the file and its metadata.  



