// liballdat.cpp Douglas Leonard 2015, CUP IBS, all rights reserved.
// alldat.h Douglas Leonard 2015, CUP IBS, all rights reserved.
// Copyright (C) 2015 Doug Leonard
//
//Distributed under LGPLv2.1 (and not later versions):
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License.

//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.

//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

/*
 Read ORTEC Spc files and optionally write out to chn.  Supports floating point and integer files
 For integer Spc files, the results are bit perfect with exception of three
 undocumented bytes specifying, generally, "keV" after the calibration constants in the GammaVission files.
 these appear in memmory marked as "reserved" and the bytes are not found in the spc file.
 */
#include "alldat.h"

#include <fstream>
#include <string>
#include <iostream>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <map>
#include <memory>
#include <vector>
#include <algorithm>
#include <TFile.h>
#include <TH1F.h>
#include <TH1I.h>
#include <TTree.h>
#include <TChain.h>
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include <tuple>
#include <stdint.h>
#include "fileio.h"

#define INT_DATA 1
#define FLOAT_DATA 5

using namespace std;

/*
 Purpose: Class Initializer, especially for root branch definitions
 */
alldat::alldat() {
    // initialize Root Branch definitions:
    brvec.push_back(make_tuple("real_time_sec", &spc_realtime, "real_time_sec/F"));
    brvec.push_back(make_tuple("live_time_sec", &spc_livetime, "live_time_sec/F"));
    brvec.push_back(make_tuple("gen_evt_cnt", &gen_event, "gen_evt_cnt/I"));
    brvec.push_back(make_tuple("samp_desc", sampdesc, "sampdesc/C"));
    brvec.push_back(make_tuple("det_desc", detdesc, "detdesc/C"));
    brvec.push_back(make_tuple("spc_startdate", spc_cstartdate, "spc_cstartdate/C"));
    brvec.push_back(make_tuple("spc_starttime", spc_cstarttime, "spc_cstarttime/C"));
    brvec.push_back(make_tuple("ecal0", &ecal0, "ecal0/F"));  // usage may vary, nominally matches ortec std
    brvec.push_back(make_tuple("ecal1", &ecal1, "ecal1/F"));
    brvec.push_back(make_tuple("ecal2", &ecal2, "ecal2/F"));
    brvec.push_back(make_tuple("sigcal0", &sigcal0, "ecal0/F"));  // usage may vary, nominally matches ortec std
    brvec.push_back(make_tuple("sigcal1", &sigcal0, "sigcal1/F"));
    brvec.push_back(make_tuple("sigcal2", &sigcal0, "sigcal2/F"));
    brvec.push_back(make_tuple("istarttime", &istarttime, "arr[7]/I"));

    //  A bunch of dummy values:
    memcpy(chn_cstartsec, "00", 2);
    realtime_20ms = 0;
    livetime_20ms = 0;
    memcpy(chn_cstartdate, "01Jan001", 9);
    memcpy(chn_cstarttime, "0000", 5);
    startchan = 0;
    int offset = 32 + (chnum) * 4;
    ecal0 = 0;
    ecal1 = 1;
    ecal2 = 0;

    memcpy(detdesc, "Data", 5);
    detdesc_len = 15;
    memcpy(sampdesc, detdesc, detdesc_len);
    sampdesc_len = detdesc_len;
    // fill out alternative time formats:

    months["Jan"] = 1;
    months["Feb"] = 2;
    months["Mar"] = 3;
    months["Apr"] = 4;
    months["May"] = 5;
    months["Jun"] = 6;
    months["Jul"] = 7;
    months["Aug"] = 8;
    months["Sep"] = 9;
    months["Oct"] = 10;
    months["Nov"] = 11;
    months["Dec"] = 12;

    chn2spc(); // convert them into spc format too
    spc2UA(); // and UA

}

/*
 Purpose: Converts ortec spc header "word number" to C memory byte offset
 turned out futile since every record is indexed differently.
 */
int alldat::w2b(int rec) {
    return (rec - 1) * 2;  // two byte records, C starts at 0, not 1
}

/*
 Purpose:Reads ORTEC .spc file named alldat.infilename into memory and maps variables.
 Presently reads only data relevant to .chn files.
 Conversions to chn variables are performed as needed here since
 data in these formats could potentially be useful before output.
 Reads integer and floating point Spc files
 and will convert to fill both data types.
 */
int alldat::readspc() {
// just read the whole file into memory
    short int *test;
    int i;
    char *testc;
    short int *spctrp_ptr;  // record number of spectrum start
    short int *acqirp_ptr;  // acquisition information record pointer
    short int *samdrp_ptr;  // sample description record pointer
    short int *detdrp_ptr;  // det desription record pointer
    short int *caldes_ptr;  // calibration description record pointer
    short int *calrec1_ptr;  // calibration  record 1 pointer
    int retval = 0;

    file_mem.reset();
    file_mem = readbinfile(infilename); // take ownership of the memory
    if (spcmemblock = file_mem.get()) { // assignment intentional.
        retval = 1;   // easier to use raw pointer for pointer arithmetic.
    } else {
        return 0;
    }
//
// And now just map our variable pointers to the memory block
//
// get file location of data records in multiple of 128 byte records:
    spctrp_ptr = (short*) (spcmemblock + w2b(31));  // stored in word offset 31
    acqirp_ptr = (short*) (spcmemblock + w2b(5));
    samdrp_ptr = (short*) (spcmemblock + w2b(6));
    detdrp_ptr = (short*) (spcmemblock + w2b(7));
    caldes_ptr = (short*) (spcmemblock + w2b(17));
    calrec1_ptr = (short*) (spcmemblock + w2b(18));

//  for (int i=0;i<128;i++){
//    cout << ":" << *(testc+i) << ": \n";
//  }
// get the header data
    memcpy(&ft1, spcmemblock + w2b(1), 2); // file type .. should be one, two byte short int.
    if (ft1 != 1) {
        cout << "Wrong file type (INFTYP)  \n";
        return 0;
//        exit(EXIT_FAILURE);
    }
    memcpy(&chnum, spcmemblock + w2b(33), 2); // number of channels, 2 byte short
    memcpy(&startchan, spcmemblock + w2b(34), 2); // first physical channel, 2 ybte short

    memcpy(&spc_realtime, spcmemblock + w2b(46), 4); // 4 byte float
// alternative notation if we ever need to cast to doubles for some reason.
//    spc_real_realtime_ptr=*(uint32_t*)(spcmemblock + w2b(46)); // 4 byte float

    memcpy(&spc_livetime, spcmemblock + w2b(48), 4); // 4 byte float

    memcpy(&seg, spcmemblock + w2b(43), 2);  //2 byte short int
    memcpy(&mca, spcmemblock + w2b(42), 2);  // 2 byte short int

// Determine the data type and read the data
    memcpy(&ft2, spcmemblock + w2b(2), 2); // two byte short int

    if (ft2 == INT_DATA) {  // integer data
        cout << "   Parsing integer Spc data \n";
        datahist = (int*) (spcmemblock + (128) * (*spctrp_ptr - 1)); // the data, 128 byte records
// convert to float data.
        int2float();
    } else if (ft2 == FLOAT_DATA) {
        cout << "     Parsing floating point Spc data \n";
        fdatahist = (float*) (spcmemblock + (128) * (*spctrp_ptr - 1)); // the data, 128 byte records
// convert to integer data
        float2int();
    } else {
        cout << "Invalid data type (FILTYP) \n";
        return 0;
//        exit(EXIT_FAILURE);
    }

// Find data in records.  We don't need a C++ code to add 1 and 17 but the
//       funny index math is meant to expose the ortec documented indeces
//       which are unfortuntely referenced differently for every record and header.
//

// Acquisition record data:  ( this record indexed by byte starting at 1
    memcpy(&defspec[0], spcmemblock + (128) * (*acqirp_ptr - 1) - 1 + 1, 10); // 10 byte char array
    memcpy(&spc_cstartdate[0], spcmemblock + (128) * (*acqirp_ptr - 1) - 1 + 17, 10); // 10 byte char array
    memcpy(&spc_cstarttime[0], spcmemblock + (128) * (*acqirp_ptr - 1) - 1 + 29, 8); // 8 byte char array
//// The following gets times from acq record in rounded seconds
////  But the spc_real_livetime above has more precision, which does carry over
//// to the chn int because that counts 20 ms units not seconds.
//  spc_acq_char_livetime_ptr=(char *)(spcmemblock+(128)*(*acqirp_ptr-1)-1+39);
//  spc_acq_char_realtime_ptr=(char *)(spcmemblock+(128)*(*acqirp_ptr-1)-1+49);
//   char tmp_livetime[11];
//   char tmp_realtime[11];
//   memcpy(tmp_livetime,spc_acq_char_livetime_ptr,10);
//   memcpy(tmp_realtime,spc_acq_char_realtime_ptr,10);
//   tmp_livetime[10]='\0';
//   tmp_realtime[10]='\0';

// Calibration record data: ( this record indexed by 2 byte words starting at 1)
    memcpy(&ecal0, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 11, 4); // 4 byte float
    memcpy(&ecal1, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 13, 4); // 4 byte float
    memcpy(&ecal2, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 15, 4); // 4 byte float
    memcpy(&sigcal0, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 17, 4); // 4 byte float
    memcpy(&sigcal1, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 19, 4); // 4 byte float
    memcpy(&sigcal2, spcmemblock + (128) * (*calrec1_ptr - 1) - 2 + (2) * 21, 4); // 4 byte float

//  cout << hex << ecal0_ptr << " : " << ecal1 << " : " << ecal2 << endl;

// Detector and sample description records:
//  detdesc=(char *)(spcmemblock+128*(*detdrp_ptr-1));
    memcpy(detdesc, spcmemblock + 128 * (*detdrp_ptr - 1), 63);
//  sampdesc=(char *)(spcmemblock+128*(*samdrp_ptr-1));
    memcpy(&sampdesc[0], spcmemblock + 128 * (*samdrp_ptr - 1), 63);

    i = 63;
    while ((i != 0) && (*(detdesc + i - 1) == ' ')) {
        i--;
    }
    detdesc_len = i;
    detdesc[i] = 0;
    i = 63;
    while ((i != 0) && (*(sampdesc + i - 1) == ' ')) {
        i--;
    }
    sampdesc_len = i;
    sampdesc[i] = 0;
    spc2UA();
    spc2chn();

//    cout << "   Realtime [sec]: " << int_realtime / 50 << '\n';
//    cout << "   Livetime [sec]: " << int_livetime / 50 << '\n';
//   cout << "   Realtime from spc acq record: " << tmp_realtime << '\n';
//   cout << "   Livetime from spc acq record: " << tmp_livetime << '\n';
//    cout << "   Number of channels: " << chnum_ptr << '\n';
    cout << "   First Data Record #: " << *spctrp_ptr
            << "    Total byte offset:" << 128 * (*spctrp_ptr - 1) << '\n';
    cout << "    acquisisition record #:" << *acqirp_ptr
            << "    Total byte offset:" << 128 * (*acqirp_ptr - 1) << '\n';

    return 1;
}

/*
 Purpose: writes class data to chn file
 It should be called after readchn() and optionally custom data manipulations.
 */

void alldat::writechn() {
    short int type;  // 2 bytes
    int i, pad;
    short int j;
    char cj;

    cout << "Writing data to file: " << outfilename;
//  print_summary();

//  SPC file use real values for time and chn uses int:

    ofstream file(outfilename.c_str(), ios::out | ios::binary | ios::trunc);
    if (file.is_open()) {
// total file sizew is 5112 bytes, so one could pre-write 512 and then do random access seeks.
// for now, just doing sequential writes:
        file.seekp(0, ios::beg); // just make sure
        type = -1; // defined value
        file.write((char*) &type, 2);  // offset 0 defines as -1
        file.write((char*) &mca, 2);  // offset 2
        file.write((char*) &seg, 2);   // offset 4
        file.write((char*) &chn_cstartsec, 2); // offset 6
        file.write((char*) &realtime_20ms, 4); //offset 8
        file.write((char*) &livetime_20ms, 4); //offset 12
        file.write((char*) &chn_cstartdate, 8); //offset 16
        file.write((char*) &chn_cstarttime, 4); //offset 24
        file.write((char*) &startchan, 2); //offset 28
        file.write((char*) &chnum, 2); //offset 30
//  write the histogram
        file.write((char*) datahist, (chnum) * 4); //offset 32
// remaining offsets are relative to start of end data
        j = -102;
        file.write((char*) &j, 2); // offset 0, required value, hex: FF9A (9AFF), serves as end record marker
        j = 0;
        file.write((char*) &j, 2); // offset 2, reserved value
// assigning default calibration values:
        file.write((char*) &ecal0, 4); // offset 4, energy cal intercept
        file.write((char*) &ecal1, 4); // offset 8, energy cal slope
        file.write((char*) &ecal2, 4); // offset 12 energy cal quad
        file.write((char*) &sigcal0, 4); // offset 16, peak shape cal  intercept
        file.write((char*) &sigcal1, 4); // offset 20, peak shape cal slope
        file.write((char*) &sigcal2, 4); // offset 24, peak shape cal quad

// Detector and sample description not converted from spc data (but is copied from chn reads)
// clear to the programmer how or if the Spc description records (> 64 bytes) relate to the chn descriptions (<64 bytes).
// No single field in the Spc description record corresponds to a "description".
// Note, ultimately this code produces files bit-perfect equal to Maestro chn output in our use.

        cj = '\0';
        for (i = 0; i < 228; i++) {
            file.write((char*) &cj, 1);
        }
        ;                 // offset 28, length 228, reserved
        file.write((char*) &detdesc_len, 1); // offset 256, length of det description
        file.write((char*) detdesc, detdesc_len); // offset 257, det description
        for (i = 0; i < 63 - detdesc_len; i++) {
            file.write((char*) &cj, 1);
        }
        ;                 // pad out rest of 63 bytes
        file.write((char*) &sampdesc_len, 1); // offset 320, length of sample description
        file.write((char*) sampdesc, sampdesc_len); // offset 321, sample description
        for (i = 0; i < 63 - sampdesc_len; i++) {
            file.write((char*) &cj, 1);
        }
        ;                 // pad out rest of 63 bytes
        for (i = 0; i < 128; i++) {
            file.write((char*) &cj, 1);
        }
        ;                 // offset 384, reserved
        file.close();
    } else {
        cout << "Unable to open file" << outfilename << '\n';
    }
}

int alldat::readchn() {
    short int type;  // 2 bytes
    int i, pad;
    short int j;
    char cj;
    int retval = 1;
// converted line by line from working writechn(), still in comments for reference.
// just read the whole file into memory
    file_mem.reset();
    file_mem = readbinfile(infilename); // take ownership of the memory
    if (spcmemblock = file_mem.get()) { // asignment intentional.
        retval = 1;   // easier to use raw pointer for pointer arithmetic.
    } else {
        return 0;
    }
//
// And now just map our variable pointers to the memory block
//

//  SPC file use real values for time and chn uses int:

// total file sizew is 5112 bytes, so one could pre-write 512 and then do random access seeks.
// for now, just doing sequential writes:
//    type=-1; // defined value
//    file.write ((char *)&type, 2);  // offset 0 defines as -1
    memcpy(&type, spcmemblock, 2);
    if (type != -1) {
        cout << "Invalid .chn file : " << infilename << '\n';
        cout << "Exiting" << 'n';
//        retval=0;
        return 0;
//        exit(EXIT_FAILURE);
    }
    memcpy(&mca, spcmemblock + 2, 2);  // two bytes short int
    memcpy(&seg, spcmemblock + 4, 2);  // two bytes short int
    memcpy(chn_cstartsec + 0, spcmemblock + 6, 2);
    memcpy(&realtime_20ms, spcmemblock + 8, 4);
    memcpy(&livetime_20ms, spcmemblock + 12, 4);
    memcpy(chn_cstartdate, spcmemblock + 16, 8);
    memcpy(chn_cstarttime, spcmemblock + 24, 4);
    memcpy(&startchan, spcmemblock + 28, 2);  // two bytes short int
    memcpy(&chnum, spcmemblock + 30, 2);  // two bytes short int
    datahist = (int*) (spcmemblock + 32);  //set the pointer to the data.
    int offset = 32 + (chnum) * 4;
// remaining offsets are relative to start of end data
//    j=-102;
//    file.write ((char *)&j,2); // offset 0, required value, hex: FF9A (9AFF), serves as end record marker
    memcpy(&j, spcmemblock + offset, 2); // two bytes
    if (j != -102) {
        cout << '\n' << "attempted to read " << chnum << " data channels." << '\n';
        cout << ".chn file has invalid end of record marker at offset : " << offset << " in file " << infilename
                << '\n';
        cout << "Exiting" << 'n';
//        retval=0;
        return 0;
//        exit(EXIT_FAILURE);
    }

    memcpy(&ecal0, spcmemblock + offset + 4, 4);  // 4 bytes float
    memcpy(&ecal1, spcmemblock + offset + 8, 4);  // 4 bytes float
    memcpy(&ecal2, spcmemblock + offset + 12, 4);  // 4 bytes float
    memcpy(&sigcal0, spcmemblock + offset + 16, 4);  // 4 bytes float
    memcpy(&sigcal1, spcmemblock + offset + 20, 4);  // 4 bytes float
    memcpy(&sigcal2, spcmemblock + offset + 24, 4);  // 4 bytes float

    memcpy(&detdesc_len, spcmemblock + offset + 256, 1);  // one byte
    memcpy(&detdesc, spcmemblock + offset + 257, (short) detdesc_len);  // det description
    detdesc[detdesc_len] = 0;
    memcpy(&sampdesc_len, spcmemblock + offset + 320, 1);  // one byte
    memcpy(&sampdesc, spcmemblock + offset + 321, (short) sampdesc_len);
    sampdesc[sampdesc_len] = 0;

// convert to float in case it's used:
    int2float();
// fill out alternative time formats:
    chn2spc();
    spc2UA();
    return 1;
}

//Use base class TH1 to copy any 1D histogram type:
void alldat::get_root_hist(TH1 *h1) {
    chnum = h1->GetNbinsX();
    fblock.reset(new float[chnum]); // allocate memory
    fdatahist = fblock.get();

    iblock.reset(new int[chnum]); // allocate memory
    datahist = iblock.get();
    for (int i = 0; i < chnum; i++) { // copy the root data
        datahist[i] = lround(h1->GetBinContent(i));
        fdatahist[i] = h1->GetBinContent(i);
        // get root baked-in calibration constant, kev across 1 channel:
        root_cal1=h1->GetBinCenter(1)-h1->GetBinCenter(0);
        // get x_offset in channels, ie as a fraction of width of one channel:
        x_offset=h1->GetBinCenter(0)/root_cal1;
        // get y value at 0 offset.
        root_cal0=h1->GetBinCenter(0)-x_offset*root_cal1;
    }
}

void alldat::make_TH1F() {
    root_type = TH1F_TYPE;
    make_TH1();
}

void alldat::make_TH1I() {
    root_type = TH1I_TYPE;
    make_TH1();
}

void alldat::make_TH1() {
    string title;
    if (sampdesc[0] != 0) {
        title = sampdesc;
    } else if (root_type == TH1F_TYPE) {
        title = "TH1F";
    } else if (root_type == TH1I_TYPE) {
        title = "TH1I";
    } else {
        title = "TH1";
    }
// **FIXME:  This could still be wrong:
    int root_chnum=chnum-1;  // we will discard 0 channel and root goes from 1 to root_chnum
    double e_low = (ecal0) + 0.5 * (ecal1); // this is lower edge of bin 1, ie at 0.5 bins from 0.
    double e_hi = ecal0 + (root_chnum + 0.5) * (ecal1);    // upper edge of root channel

    switch (root_type) {
        case TH1F_TYPE:
            roothist_ptr.reset((TH1*) (new TH1F("spec", title.c_str(), root_chnum, e_low, e_hi)));
            break;
        case TH1I_TYPE:
            roothist_ptr.reset((TH1*) (new TH1I("spec", title.c_str(), root_chnum, e_low, e_hi)));
            break;
    }
// root histograms start at 1. 0 is "underflow bin"
    for (int i = 1; i <= chnum; i++) { // copy the root data
        roothist_ptr->SetBinContent(i, fdatahist[i]);
    }
// set the public pointer
    roothist = roothist_ptr.get();
}

int alldat::readroot() {
    short int type;  // 2 bytes
    int retval = 1;
    if (histname == "") {
        cout << "No input histogram name was provided.";
        return 0;
    }
    TFile rootfile { infilename.c_str() };
    if (rootfile.IsZombie()) {
        cout << "Could not open root file " << infilename << endl;
        return 0;
    }
    TObject *histobj;

    // ROOT crashes if you just test the GET.  Why?  Because it's ROOT.
    // and this is so much more obvious right?:
    if (!rootfile.GetListOfKeys()->Contains(histname.c_str())) {
        cout << "Could not open root histogram " << histname << " in file " << infilename << endl;
        return 0;
    }

    histobj = rootfile.Get(histname.c_str());
    if (histobj->InheritsFrom(TH1::Class())) {
        TH1 *h1 = (TH1*) histobj;
        get_root_hist(h1);
    } else {    // nothing worked
        cout << "The specified histogram name " << histname << " does not correspond to a TH1-derived object in file "
                << infilename << endl;
        return 0;
    }

// read whatever real metadata we can find:
    TChain metadata("metadata");
    ecal1 = 0;
    if (metadata.Add(infilename.c_str())) {

        // Check if branches exist before capturing them.
        // See alldat::alldat() above for branch definitions.
        for (auto &it : brvec) {
            cout << get<0>(it).c_str();
            TObject *br = metadata.GetListOfBranches()->FindObject(get<0>(it).c_str());
            if (br) {
                metadata.SetBranchAddress(get<0>(it).c_str(), get<1>(it));
            }
        }
        metadata.GetEntry(0); // and that should retrieve it.
    }
    // if no calibration found, grab it from the root histogram itself if possible.
    if (ecal1 == 0){
        ecal0=root_cal0;
        ecal1=root_cal1;
    }

    spc2chn();
    spc2UA();
    return retval;
}

void alldat::writeroot() {
    cout << "Writing data to file: " << outfilename << endl;
    make_TH1F();
    TFile root_file(outfilename.c_str(), "RECREATE");
    TTree metadata("metadata", "holds summary data, livetime, etc");

    // See alldat::alldat() above for branch definitions.
    for (auto &it : brvec) {
        if (get<2>(it) != "") { //maybe unnecessary
            metadata.Branch(get<0>(it).c_str(), get<1>(it), get<2>(it).c_str());
        } else {
            metadata.Branch(get<0>(it).c_str(), get<1>(it));
        }
    }
    metadata.Fill();
//    metadata.Write();
    roothist->Write();
    root_file.Write();
    root_file.Close();
}

void alldat::int2float() {
    int tint;  // temp variables
    float tfloat;
    fblock.reset(new float[chnum]);  // allocate memory
    fdatahist = fblock.get();
    for (int i = 0; i < chnum; i++) {
        memcpy(&tint, datahist + i, 4); // copy memory byte to an int
        tfloat = (float) tint; // rounding assuming >0
        memcpy(fdatahist + i, &tfloat, 4); // copy to int memory block
    }
}
void alldat::float2int() {
    int tint;  // temp variables
    float tfloat;
// convert to float in case it's used:
    iblock.reset(new int[chnum]);  // allocate memory
    datahist = iblock.get();
    for (int i = 0; i < chnum; i++) {
        memcpy(&tfloat, fdatahist + i, 4); // copy memory byte to an int
        tint = (int) tfloat; // rounding assuming >0
        memcpy(datahist + i, &tint, 4); // copy to int memory block
    }
}

void alldat::writespe() {
    spc2UA();
    int *is = &istarttime[0]; // abreviation
    ofstream file(outfilename.c_str(), ios::out | ios::trunc);
    file << "File created by liballdat" << endl;
    file << "$DET_DESC:" << endl; // unofficial fields are officially allowed.
    file << detdesc << endl;
    file << "$SPEC_ID:" << endl;
    file << sampdesc << endl;
    file << "$DATE_MEA:" << endl;
    file << std::setfill('0')
            << std::setw(2) << is[1] << std::setw(1) << "/"   // MM/
            << std::setw(2) << is[2] << std::setw(1) << "/"   // DD/
            << std::setw(4) << is[0] << std::setw(1) << " "   // "YYYY "
            << std::setw(2) << is[3] << std::setw(1) << ":"   // "HH:"
            << std::setw(2) << is[4] << std::setw(1) << ":"   // "MM:"
            << std::setw(2) << is[5] << std::setw(1) << " "   // "SS:"
            << endl;
    file << "$MEAS_TIM:" << endl;
    file << spc_livetime << " " << spc_realtime << endl;
    file << "$GEN_EVNTS:" << endl;  // custom option
    file << gen_event << endl;
    file << "$ENER_FIT:" << endl;
    file << ecal0 << " " << ecal1 << endl;
    file << "$DATA:" << endl;
    //Poorly worded documentation indicates # of channels, but real world seems to say max channel num.
    file << 0 << " " << chnum - 1 << endl;
    for (int i = 0; i < chnum; i++) {
        file << setfill(' ') << std::setw(14) << std::right << fdatahist[i] << endl;
    }
    file.close();
}

int alldat::readspe() {
    int retval{0};
    string line;
    stringstream ss;
    ifstream file(infilename.c_str(), ios::in);
    string temp;
    string mon, dd, decade, yy;
    int imon;
    while (getline(file, line)) {
        string nextline="";
        getline(file, nextline);
        ss.clear();
        ss.str("");
        ss << std::setw(0);
        ss.str(nextline);
        if (line == "$DET_DESC:") {
            memcpy(detdesc,nextline.c_str(),min((int)nextline.length(),(int)(DESCLEN)));
        } else if (line == "$SPEC_ID:") {
            memcpy(sampdesc,nextline.c_str(),min((int)nextline.length(),(int)(DESCLEN)));
        } else if (line == "$ENER_FIT:") {
            ss >> ecal0 >> ecal1;
        } else if (line == "$ENER_FIT:") {
            ss >> ecal0 >> ecal1;
        } else if (line == "$MEAS_TIM:") {
            ss >> spc_livetime >> spc_realtime;
        } else if (line == "$GEN_EVNTS:") { // custom option
            ss >> gen_event;
        } else if (line == "$DATE_MEA:") {
            ss
                    >> std::setw(2) >> imon >> std::setw(1) >> temp   // MM/
                    >> std::setw(2) >> dd >> std::setw(1) >> temp   // DD/
                    >> std::setw(2) >> decade >> yy ;   // "YYYY "
            ss >> spc_cstarttime; // direct copy
            for (auto m : months) { // reverse lookup of month to string.
                if (m.second == imon) {
                    mon = m.first;
                }
            }
            ss.str(""); // reformat date to spc format.
            ss << std::setfill('0')
                    << std::setw(2) << dd << std::setw(1) << "-"   // DD/
                    << std::setw(3) << mon << std::setw(1) << "-"   // MMM/
                    << std::setw(2) << yy << std::setw(1) << "1";   // "YY1"
            ss >> spc_cstartdate;
        } else if (line == "$DATA:") {
            ss >> startchan >> chnum;
            chnum++;
            fblock.reset(new float[chnum]); // reallocate
            iblock.reset(new int[chnum]); // reallocate
            fdatahist=fblock.get();
            datahist=iblock.get();
            for (int i = startchan; i < chnum; i++) {
                getline(file, nextline);
                ss.clear();
                ss.str("");
                ss << nextline;
                ss >> fdatahist[i];
            //*TODO should probably abandon integer interface altogether
            //  root does.  It's only needed during accumulation.
                datahist[i]=fdatahist[i];
             }
            retval=1;  // got all spectrum data.
        }
    }
    spc2chn();
    spc2UA();
    return retval;
}

void alldat::spc2chn() {
//conversions...
// copy start time to chn format

// copy to chn format
    memcpy(chn_cstarttime + 0, spc_cstarttime + 0, 2);
    memcpy(chn_cstarttime + 2, spc_cstarttime + 3, 2);
    memcpy(chn_cstartsec + 0, spc_cstarttime + 6, 2);
    memcpy(chn_cstartdate + 0, spc_cstartdate + 0, 2);
    memcpy(chn_cstartdate + 2, spc_cstartdate + 3, 3);
    memcpy(chn_cstartdate + 5, spc_cstartdate + 7, 3);

    // convert realtime to integer of 20ms units as per chn format
    realtime_20ms = int(spc_realtime * 50 + 0.5);
    livetime_20ms = int(spc_livetime * 50 + 0.5);
}

void alldat::chn2spc() {
// copy chn time/date format to spc
//    memcpy(&spc_cstarttime[0], &spc_cstarttime[0], 8);
//    memcpy(&spc_cstartdate[0], &spc_cstartdate[0], 10);

    memcpy(spc_cstarttime + 0, chn_cstarttime + 0, 2);
    memcpy(spc_cstarttime + 3, chn_cstarttime + 2, 2);
    memcpy(spc_cstarttime + 6, chn_cstartsec + 0, 2);
    memcpy(spc_cstartdate + 0, chn_cstartdate + 0, 2);
    memcpy(spc_cstartdate + 3, chn_cstartdate + 2, 3);
    memcpy(spc_cstartdate + 7, chn_cstartdate + 5, 3);

// add standard separators:
    spc_cstarttime[2] = ':';
    spc_cstarttime[5] = ':';
    spc_cstartdate[2] = ':';
    spc_cstartdate[6] = ':';

    // convert acquisition time to seconds from 20ms units and store result:
    spc_realtime = realtime_20ms / 50.0;
    spc_livetime = livetime_20ms / 50.0;
}

void alldat::spc2UA() {
    // parses combined spc date/time formats into individual fields.
    // Relies on use of non-integer separators which serve as terminators for atoi() library call.
    // Thus chn format won't work directly since it doesn't have separators.
    // use chn2spc first.

    // make a temp copy, one extra element for termination(separator)
    char tempstarttime[9];
    char tempstartdate[10];

    memcpy(tempstarttime + 0, spc_cstarttime + 0, 8);
    memcpy(tempstartdate + 0, spc_cstartdate + 0, 10);

// gaurantee separators are not digits (atoi stops on non-digits)
    tempstarttime[2] = ':';
    tempstarttime[5] = ':';
    tempstarttime[8] = ':';

    tempstartdate[2] = ':';
    tempstartdate[6] = ':';
    tempstartdate[9] = ':';

    char temp_month[4];
// copy month string
    memcpy(temp_month + 0, spc_cstartdate + 3, 3);
    temp_month[3] = '\0';

    //0123456789
    //DD-MMM-YY1  spc_cstartdate  // Month as letters ex: Jan
    //DDMMMYY1    chn_cstartdate // Month as letters ex: Jan

    //HH:MM:SS    spc_cstarttime
    //HHMM        chn_cstarttime
    //SS          chn_cstartsec
    //int istarttime[7] ={YEAR, MONTH, DAY, HOUR, MIN, SEC, 100TH'S??}

    const char *day_ptr = &tempstartdate[0];
    const char *yr_ptr = &tempstartdate[7];
    const char *hr_ptr = &tempstarttime[0];
    const char *min_ptr = &tempstarttime[3];
    const char *sec_ptr = &tempstarttime[6];

    // get all the integers into UA format
    istarttime[0] = atoi(yr_ptr)+2000; //year
    istarttime[2] = atoi(day_ptr);    //day
    istarttime[3] = atoi(hr_ptr);    //hour
    istarttime[4] = atoi(min_ptr);    //hour
    istarttime[5] = atoi(sec_ptr);    //hour

    istarttime[1] = 1;
    auto it = months.find(temp_month);
    if (it == months.end()) {
        cout << "Invalid month string '" << temp_month << "'  Assuming 'Jan'\n";
    } else {
        istarttime[1] = it->second;
    }
}

int alldat::flexread() {
// read any of several recognized filetypes based on file extension
    std::string ext;
    ext = infilename.substr(infilename.find_last_of(".") + 1);
    // convert to lower case:
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    int retval = 0;
    if (ext == "chn") {
        retval = readchn();
    } else if (ext == "spc") {
        retval=readspc();
    } else if (ext == "root") {
        retval=readroot();
    } else if (ext == "spe") {
        retval=readspe();
    } else {
        cout << "Input file type with extension ." << ext << " is not supported" << endl;
    }
    // return 1 if no file matches.
    return retval;
}

void alldat::flexwrite() {
// read any of several recognized filetypes based on file extension
    std::string ext;
    ext = outfilename.substr(outfilename.find_last_of(".") + 1);
    // convert to lower case:
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    int retval = 0;
    if (ext == "chn") {
        writechn();
    } else if (ext == "root") {
        writeroot();
    } else if (ext == "spe") {
        writespe();
    } else {
        cout << "Output file type with extension ." << ext << " is not supported" << endl;
    }

    // return 1 if no file matches.
//    return retval;
}

void alldat::print_summary() {
    chn2spc();
    int i;
    cout << endl;
    cout << "Original file: " << infilename << endl;
    cout << "Detector Description: " << detdesc << endl;
    cout << "Sample Description: " << sampdesc << endl;
// for dbg    cout << "SPC cstartdate" << spc_cstartdate << endl;
    cout << "start date: ";
    for (i = 0; i < 8; i++) {
        cout << chn_cstartdate[i];
    }
    cout << "  -->  (DDMonthYYX  X should be 1)  \n";
    cout << "start time: ";
    for (i = 0; i < 2; i++) {
        cout << chn_cstarttime[i];
    }
    cout << ":";
    for (i = 2; i < 4; i++) {
        cout << chn_cstarttime[i];
    }
    cout << ":";
    for (i = 0; i < 2; i++) {
        cout << chn_cstartsec[i];
    }
    cout << endl;
    cout << "real time: " << spc_realtime << endl;
    cout << "live time: " << spc_livetime << endl;
    cout << "Energy calibration parameters, ecal0: " << ecal0 << " ecal1: " << ecal1 << " ecal2: " << ecal2 << endl;
    cout << "Gnerated events: " << gen_event << endl;
    cout << "Channels " << chnum << "\n\n";

// For debugging:
//  cout << istarttime[0] << "/" << istarttime[1] << "/" << istarttime[2] << " ";
//  cout << istarttime[3] << ":" << istarttime[4] << ":" << istarttime[5] << endl;
}

class alldat& alldat::operator+=(class alldat &rhs) {
// no consistency checks between data sizes here.
// At your own risk!
    for (int j = 0; j < this->chnum; j++) {
        *(this->datahist + j) += *(rhs.datahist + j);
    }
    this->spc_realtime += rhs.spc_realtime;
    this->spc_livetime += rhs.spc_livetime;
    this->realtime_20ms += rhs.spc_realtime * 50 + 0.5;
    this->livetime_20ms += rhs.spc_livetime * 50 + 0.5;
    this->gen_event += rhs.gen_event;
    return *this;
}

class alldat& alldat::operator-=(class alldat &rhs) {
// no consistency checks between data sizes here.
// At your own risk!
    for (int j = 0; j < this->chnum; j++) {
        *(this->datahist + j) -= *(rhs.datahist + j);
    }
// Ideally we should add the real valued live time which is in units of 20ms
// and then redo the conversion.
// This way will produce a small error :P, oh well...
    this->spc_realtime -= rhs.spc_realtime;
    this->spc_livetime -= rhs.spc_livetime;
    this->realtime_20ms -= rhs.spc_realtime * 50 + 0.5;
    this->livetime_20ms -= rhs.spc_livetime * 50 + 0.5;
    this->gen_event -= rhs.gen_event;
    return *this;
}

