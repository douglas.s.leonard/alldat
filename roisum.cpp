//spc2chn Douglas Leonard, 2015, CUP IBS, all rights reserved

// Copyright (C) 2015 Doug Leonard
//
//Licensed under lgpl, v2.1-only, with the exception that no restrictions are placed
//on the licenses of the linked code or binaries  (such as reverse engineering restrictions),
//and that this same exception applies to all modified versions of this work.

#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>

#include "alldat.h"


using namespace std;
// Outputs sum of ROI region defined by two integers in submin.txt or entire spectrum if not given.
// accepts all supported input file formats distinguishable by extension, chn and spc when this comment was written.

int main(int argc, char* argv[]) {
  if (argc != 2) {
    cout << argc << "     Usage:roisum  input1"<< endl;
    cout << "Input can be any supported file type ex: chn, spc "<< endl;
    cout << endl;
    exit(1);
  }

  alldat sumdata;

  alldat *data[argc-2]; // 0 is command and last is output name
  sumdata.infilename=argv[1];
  sumdata.flexread();
  sumdata.print_summary();


// If no ROI data, calculate full channel sum.
  int lowbin=0, hibin, binsum=0;
  hibin=sumdata.chnum;

  ifstream sumROIfile ("sumbin.txt");
  if (sumROIfile.is_open()) {
      sumROIfile >> lowbin;
      sumROIfile >> hibin;
  }
  sumROIfile.close();

// no consistency checks between data sizes here.
// At your own risk!
// Tally ROI sum:
  for (int j=0; j< sumdata.chnum; j++) {
        if ( j >= lowbin && j <= hibin) {
         binsum+=*(sumdata.datahist+j);
        }
  }

  sumdata.outfilename=argv[argc-1];
  cout << "ROI: " << lowbin << " : " << hibin << '\n';
  cout << "binsum:  " << binsum << '\n';
}
